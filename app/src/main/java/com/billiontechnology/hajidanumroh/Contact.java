package com.billiontechnology.hajidanumroh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Contact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
    }
    //tmbh activity Back
    public void startDoa(View view)
    {
        startActivity(new Intent(this, Doa.class));
    }
}
