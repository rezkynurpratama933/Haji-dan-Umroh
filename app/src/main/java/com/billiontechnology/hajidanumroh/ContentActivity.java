package com.billiontechnology.hajidanumroh;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by admin on 24-Oct-17.
 */

public class ContentActivity extends AppCompatActivity {
    Integer[] image = {R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher};
    String[] name = {"Doa - doa", "Rukun Haji", "Rukun Umroh", "Contact Us"};
    ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        listview = (ListView) findViewById(R.id.listview2);

        Customlist adapter = new Customlist(ContentActivity.this, name, image);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Tambah
                if(position==0){
                    Toast.makeText(ContentActivity.this,"Doa - doa",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(ContentActivity.this,Doa.class);
                    startActivity(intentku);
                }
                if(position==1){
                    Toast.makeText(ContentActivity.this,"Rukun Haji",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(ContentActivity.this,RknHaji.class);
                    startActivity(intentku);
                }
                if(position==2){
                    Toast.makeText(ContentActivity.this,"Rukun Umroh",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(ContentActivity.this,RknUmroh.class);
                    startActivity(intentku);
                }
                if(position==3){
                    Toast.makeText(ContentActivity.this,"Contact Us",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(ContentActivity.this,Contact.class);
                    startActivity(intentku);
                }
            }


        });
    }
    //tmbh activity Back
    public void startDoa(View view)
    {
        startActivity(new Intent(this, MainActivity.class));
    }
}
