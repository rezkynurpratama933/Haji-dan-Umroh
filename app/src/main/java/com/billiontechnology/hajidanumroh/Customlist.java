package com.billiontechnology.hajidanumroh;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by admin on 20-Oct-17.
 */

public class Customlist extends ArrayAdapter<String> {
    public final Activity context;
    public final String[] name;
    public final Integer[] image;

    public Customlist(Context context, String[] name, Integer[] image) {
        super(context, R.layout.item, name);
        this.context = (Activity) context;
        this.name = name;
        this.image = image;
    }

    @Override
    public View getView (int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.item, null, true);
        ImageView imgView = (ImageView) rowView.findViewById(R.id.imageView);
        TextView txtView = (TextView) rowView.findViewById(R.id.textview);
        imgView.setImageResource(image[position]);
        txtView.setText(name[position]);
        return rowView;
    }
}
