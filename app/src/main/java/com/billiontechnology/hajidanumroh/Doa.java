package com.billiontechnology.hajidanumroh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class Doa extends AppCompatActivity {

    Integer[] image = {R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher,R.mipmap.ic_launcher
    ,R.mipmap.ic_launcher,R.mipmap.ic_launcher,R.mipmap.ic_launcher,R.mipmap.ic_launcher,R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,R.mipmap.ic_launcher,R.mipmap.ic_launcher,};

    String[] name = {"Niat Haji Dan Umroh", "Bacaan Talbiyah", "Bacaan Sesudah  Sholawat", "Doa Melihat Ka'bah",
            "Doa Thawaf Dari Hajar Aswad", "Doa Thawaf Sampai Yamin","Doa Thawaf Menuju Multazam"
    ,"Shalat Di Hijer Ismail Dan Sayyidul","Doa Minum Air Zam-zam","Doa Menuju Bukit Shfa","Doa Diatas Bukit Shafa"
    ,"Doa Menuju Shafa","Di Marwa"};
    ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doa);


        listview = (ListView) findViewById(R.id.lstDoa);

        Customlist adapter = new Customlist(Doa.this, name, image);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Tambah
                if(position==0){
                    Toast.makeText(Doa.this,"Niat Haji Dan Umroh",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,Niat.class);
                    startActivity(intentku);
                }
                if(position==1){
                    Toast.makeText(Doa.this,"Bacaan Talbiyah",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,BacaanTalbiyah.class);
                    startActivity(intentku);
                }
                if(position==2){
                    Toast.makeText(Doa.this,"Bacaan Sesudah Sholawat",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,BcSesudahSholawat.class);
                    startActivity(intentku);
                }
                if(position==3){
                    Toast.makeText(Doa.this,"Doa Melihat Ka'bah",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,DoaMlhtKbh.class);
                    startActivity(intentku);
                }
                if(position==4){
                    Toast.makeText(Doa.this,"Doa Thawaf Dari Hajar Aswad",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,DoaTwfHajarAswad.class);
                    startActivity(intentku);
                }
                if(position==5){
                    Toast.makeText(Doa.this,"Doa Thawaf Sampai Yaman",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,TwfYaman.class);
                    startActivity(intentku);
                }
                if(position==6){
                    Toast.makeText(Doa.this,"Doa Thawaf Menuju Multazam",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,TwfMultazam.class);
                    startActivity(intentku);
                }
                if(position==7){
                    Toast.makeText(Doa.this,"Sholat Di Hijer Ismail Dan Doa Sayyidul",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,SltHijer.class);
                    startActivity(intentku);
                }
                if(position==8){
                    Toast.makeText(Doa.this,"Doa Minum Air Zamzam",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,MnmAirZamzam.class);
                    startActivity(intentku);
                }

                if(position==9){
                    Toast.makeText(Doa.this,"Doa Menuju Bukit Shafa",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,BktShafa.class);
                    startActivity(intentku);
                }
                if(position==10){
                    Toast.makeText(Doa.this,"Doa Diatas Bukit Shafa",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,DiatasBktShafa.class);
                    startActivity(intentku);
                }
                if(position==11){
                    Toast.makeText(Doa.this,"Doa Menuju Shafa",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,MenujuShafa.class);
                    startActivity(intentku);
                }

                if(position==12){
                    Toast.makeText(Doa.this,"Kegiatan Di Marwa",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Doa.this,KegiatanMarwa.class);
                    startActivity(intentku);
                }
            }


        });
    }



    //tmbh activity Back
    public void startDoa(View view)
    {
        startActivity(new Intent(this, MainActivity.class));
    }
}

