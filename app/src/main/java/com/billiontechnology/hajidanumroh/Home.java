package com.billiontechnology.hajidanumroh;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.support.design.widget.NavigationView;

import static com.billiontechnology.hajidanumroh.R.layout.item;

public class Home extends AppCompatActivity {
    Integer[] image = {R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher};
    String[] name = {"Doa - doa", "Rukun Haji", "Rukun Umroh", "Contact Us"};
    ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        listview = (ListView) findViewById(R.id.listview);

        Customlist adapter = new Customlist(Home.this, name, image);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Tambah
                if(position==0){
                    Toast.makeText(Home.this,"Doa - doa",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Home.this,Doa.class);
                    startActivity(intentku);
                }
                if(position==1){
                    Toast.makeText(Home.this,"Rukun Haji",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Home.this,RknHaji.class);
                    startActivity(intentku);
                }
                if(position==2){
                    Toast.makeText(Home.this,"Rukun Umroh",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Home.this,RknUmroh.class);
                    startActivity(intentku);
                }
                if(position==3){
                    Toast.makeText(Home.this,"Contact Us",Toast.LENGTH_SHORT).show();
                    Intent intentku= new Intent(Home.this,Contact.class);
                    startActivity(intentku);
                }
            }


        });
    }



    //tmbh activity Back
    public void startDoa(View view)
    {
        startActivity(new Intent(this, MainActivity.class));
    }
}


//    public void onListItemClick(ListView l, View v, int position, long id) {
//        Intent intent = new Intent(Home.this, Doa.class);
//        startActivity(intent);

//                Toast.makeText(Home.this, "Clicked on:" + position, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//}